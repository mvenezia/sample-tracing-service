package sampletracingservice

import (
	popularityPB "gitlab.com/mvenezia/popularity-service/pkg/generated/api"
	twitchPB "gitlab.com/mvenezia/twitch-service/pkg/generated/api"
	pb "gitlab.com/mvenezia/sample-tracing-service/pkg/generated/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func (s *Server) GetItemDetails(ctx context.Context, in *pb.GetItemDetailsMsg) (*pb.GetItemDetailsReply, error) {
	popularityConn, popularityContext, err := createGRPCConn(ctx, "popularity-service:8080", injectHeadersIntoMetadata(ctx))
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	popularityClient := popularityPB.NewPopularityServiceAPIClient(popularityConn)
	popularity, err := popularityClient.GetPopularity(popularityContext, &popularityPB.GetPopularityMsg{Item: in.ItemId})
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}

	twitchConn, twitchContext, err := createGRPCConn(ctx, "twitch-service:80", injectHeadersIntoMetadata(ctx))
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	twitchClient := twitchPB.NewTwitchServiceAPIClient(twitchConn)
	twitchAnswer, err := twitchClient.GetStreamInformation(twitchContext, &twitchPB.GetStreamMsg{StreamId: in.ItemId})
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}

	return &pb.GetItemDetailsReply{PopularityScore: popularity.Popularity, Name: twitchAnswer.Name, Live: twitchAnswer.Live}, nil
}

const (
	prefixTracerState  = "x-b3-"
	zipkinTraceID      = prefixTracerState + "traceid"
	zipkinSpanID       = prefixTracerState + "spanid"
	zipkinParentSpanID = prefixTracerState + "parentspanid"
	zipkinSampled      = prefixTracerState + "sampled"
	zipkinFlags        = prefixTracerState + "flags"
	requestID          = "x-request-id"
	otSpanContext      = "x-ot-span-context"
)

var otHeaders = []string{
	zipkinTraceID,
	zipkinSpanID,
	zipkinParentSpanID,
	zipkinSampled,
	zipkinFlags,
	requestID,
	otSpanContext,
}

func injectHeadersIntoMetadata(ctx context.Context) metadata.MD {
	headers, _ := metadata.FromIncomingContext(ctx)
	pairs := []string{}
	for _, h := range otHeaders {
		if v := headers.Get(h); len(v) > 0 {
			pairs = append(pairs, h, v[0])
			logger.Infof("Setting %s to %s\n", h, v[0])
		}
	}
	return metadata.Pairs(pairs...)
}

// ctx is the incoming gRPC request's context
// addr is the address for the new outbound request
func createGRPCConn(ctx context.Context, addr string, md metadata.MD) (*grpc.ClientConn, context.Context, error) {
	opts := []grpc.DialOption{grpc.WithInsecure()}

	outgoingCtx := metadata.NewOutgoingContext(context.Background(), md)

	conn, err := grpc.DialContext(outgoingCtx, addr, opts...)
	if err != nil {
		logger.Errorf("Failed to connect to application addr: ", err)
		return nil, nil, err
	}
	return conn, outgoingCtx, nil
}
