package sampletracingservice

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/sample-tracing-service/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.sample-tracing-service", loggo.INFO)
}
