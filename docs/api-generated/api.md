# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [api.proto](#api.proto)
    - [GetItemDetailsMsg](#sampletracingservice.GetItemDetailsMsg)
    - [GetItemDetailsReply](#sampletracingservice.GetItemDetailsReply)
    - [GetVersionMsg](#sampletracingservice.GetVersionMsg)
    - [GetVersionReply](#sampletracingservice.GetVersionReply)
    - [GetVersionReply.VersionInformation](#sampletracingservice.GetVersionReply.VersionInformation)
  
  
  
    - [SampleTracingServiceAPI](#sampletracingservice.SampleTracingServiceAPI)
  

- [api.proto](#api.proto)
    - [GetItemDetailsMsg](#sampletracingservice.GetItemDetailsMsg)
    - [GetItemDetailsReply](#sampletracingservice.GetItemDetailsReply)
    - [GetVersionMsg](#sampletracingservice.GetVersionMsg)
    - [GetVersionReply](#sampletracingservice.GetVersionReply)
    - [GetVersionReply.VersionInformation](#sampletracingservice.GetVersionReply.VersionInformation)
  
  
  
    - [SampleTracingServiceAPI](#sampletracingservice.SampleTracingServiceAPI)
  

- [Scalar Value Types](#scalar-value-types)



<a name="api.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="sampletracingservice.GetItemDetailsMsg"></a>

### GetItemDetailsMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| item_id | [string](#string) |  | The item id |






<a name="sampletracingservice.GetItemDetailsReply"></a>

### GetItemDetailsReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Item name |
| popularity_score | [float](#float) |  | the score of the item |
| live | [bool](#bool) |  | Is the stream live? |






<a name="sampletracingservice.GetVersionMsg"></a>

### GetVersionMsg







<a name="sampletracingservice.GetVersionReply"></a>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#sampletracingservice.GetVersionReply.VersionInformation) |  | Version Information |






<a name="sampletracingservice.GetVersionReply.VersionInformation"></a>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |





 

 

 


<a name="sampletracingservice.SampleTracingServiceAPI"></a>

### SampleTracingServiceAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#sampletracingservice.GetVersionMsg) | [GetVersionReply](#sampletracingservice.GetVersionReply) | Will return version information about api server |
| GetItemDetails | [GetItemDetailsMsg](#sampletracingservice.GetItemDetailsMsg) | [GetItemDetailsReply](#sampletracingservice.GetItemDetailsReply) | Will return version information about api server |

 



<a name="api.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="sampletracingservice.GetItemDetailsMsg"></a>

### GetItemDetailsMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| item_id | [string](#string) |  | The item id |






<a name="sampletracingservice.GetItemDetailsReply"></a>

### GetItemDetailsReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Item name |
| popularity_score | [float](#float) |  | the score of the item |
| live | [bool](#bool) |  | Is the stream live? |






<a name="sampletracingservice.GetVersionMsg"></a>

### GetVersionMsg







<a name="sampletracingservice.GetVersionReply"></a>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#sampletracingservice.GetVersionReply.VersionInformation) |  | Version Information |






<a name="sampletracingservice.GetVersionReply.VersionInformation"></a>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |





 

 

 


<a name="sampletracingservice.SampleTracingServiceAPI"></a>

### SampleTracingServiceAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#sampletracingservice.GetVersionMsg) | [GetVersionReply](#sampletracingservice.GetVersionReply) | Will return version information about api server |
| GetItemDetails | [GetItemDetailsMsg](#sampletracingservice.GetItemDetailsMsg) | [GetItemDetailsReply](#sampletracingservice.GetItemDetailsReply) | Will return version information about api server |

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

