package main

import "gitlab.com/mvenezia/sample-tracing-service/cmd/sample-tracing-service/cmd"

func main() {
	cmd.Execute()
}
